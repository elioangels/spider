"use strict"
const mongoose = require("mongoose")

const Event = new mongoose.Schema()

Event.plugin(require("./plugins/event"))
Event.plugin(require("./plugins/thing"))
Event.plugin(require("../../../adon"))

module.exports = mongoose.model("Event", Event)
