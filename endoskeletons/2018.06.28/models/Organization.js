"use strict"
const mongoose = require("mongoose")

const Organization = new mongoose.Schema()

Organization.plugin(require("./plugins/organization"))
Organization.plugin(require("./plugins/thing"))
Organization.plugin(require("../../../adon"))

module.exports = mongoose.model("Organization", Organization)
