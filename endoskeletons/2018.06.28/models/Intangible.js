"use strict"
const mongoose = require("mongoose")

const Intangible = new mongoose.Schema()

Intangible.plugin(require("./plugins/intangible"))
Intangible.plugin(require("./plugins/thing"))
Intangible.plugin(require("../../../adon"))

module.exports = mongoose.model("Intangible", Intangible)
