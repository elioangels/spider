"use strict"
let mongoose = require("mongoose")
module.exports = exports = function thing(schema, options) {
  schema.add({
    additionalType: {
      type: String,
    },
    alternateName: {
      type: String,
    },
    description: {
      type: String,
    },
    disambiguatingDescription: {
      type: String,
    },
    identifier: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PropertyValue",
    },
    image: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ImageObject",
    },
    mainEntityOfPage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    name: {
      type: String,
    },
    potentialAction: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Action",
    },
    sameAs: {
      type: String,
    },
    subjectOf: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "CreativeWork",
    },
    url: {
      type: String,
    },
  })
}
