"use strict"
let mongoose = require("mongoose")
module.exports = exports = function place(schema, options) {
  schema.add({
    additionalProperty: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PropertyValue",
    },
    address: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PostalAddress",
    },
    aggregateRating: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "AggregateRating",
    },
    amenityFeature: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "LocationFeatureSpecification",
    },
    branchCode: {
      type: String,
    },
    containedInPlace: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    containsPlace: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Place",
    },
    event: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Event",
    },
    faxNumber: {
      type: String,
    },
    geo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeoCoordinates",
    },
    geospatiallyContains: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyCoveredBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyCovers: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyCrosses: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyDisjoint: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyEquals: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyIntersects: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyOverlaps: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyTouches: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    geospatiallyWithin: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "GeospatialGeometry",
    },
    globalLocationNumber: {
      type: String,
    },
    hasMap: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Map",
    },
    isAccessibleForFree: {
      type: Boolean,
    },
    isicV4: {
      type: String,
    },
    logo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ImageObject",
    },
    maximumAttendeeCapacity: {
      type: Number,
    },
    openingHoursSpecification: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OpeningHoursSpecification",
    },
    photo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ImageObject",
    },
    publicAccess: {
      type: Boolean,
    },
    review: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review",
    },
    smokingAllowed: {
      type: Boolean,
    },
    specialOpeningHoursSpecification: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "OpeningHoursSpecification",
    },
    telephone: {
      type: String,
    },
  })
}
