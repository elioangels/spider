"use strict"
const mongoose = require("mongoose")

const Action = new mongoose.Schema()

Action.plugin(require("./plugins/action"))
Action.plugin(require("./plugins/thing"))
Action.plugin(require("../../../adon"))

module.exports = mongoose.model("Action", Action)
