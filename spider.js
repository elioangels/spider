/**

#######  / _ \ ########################
##     \_\(_)/_/          elioSpider  ##
#####  _//o\\_  ######################
       /   \

 * Here's what we are working with in cheerio, scraped from SCHEMA.org...
 * ...which is different from what you'll see in page source in a browser!
 */

/**
table
  thead
    tr: TARGET SCHEMA TITLE
  thead
    tr: TARGET SCHEMA prop1
    tr: TARGET SCHEMA prop2
    tr: TARGET SCHEMA prop ...
    tr last: NEXT SCHEMA1 TITLE
  thead
    tr: NEXT SCHEMA1 prop1
    tr: NEXT SCHEMA1 prop2
    tr: NEXT SCHEMA1 prop ...
    tr last: NEXT SCHEMA3 TITLE
  thead
    tr: NEXT SCHEMA3 prop ...
  ... etc
 */
"use strict"
const cheerio = require
const fetch = require("isomorphic-fetch")
const fs = require("fs")
const Rx = require("rxjs/Rx")

const domain = "https://schema.org"

const TEMPLATES = {
  plugin: fs.readFileSync("./templates/plugin.ejs", "utf-8"),
  model: fs.readFileSync("./templates/model.ejs", "utf-8"),
}

class Spider {
  constructor(version, depth, thingsSelector, useOjectFields) {
    this._schema = {}
    this._version = version
    this._depth = depth
    this._thingsSelector = thingsSelector
    this._useOjectFields = useOjectFields
  }

  get ALL_SCHEMAS() {
    return this._schemas
  }

  get TEMPLATES() {
    return this._templates
  }

  set ALL_SCHEMAS(schemas) {
    this._schemas = schemas
  }

  /**
   * Method which spiders all the Schema.org Types.
   * @param {*object} callback To handle the data when we finish.
   */
  spider(callback) {
    // Cos we can't refer to `this` in promises.
    var self = this

    var SPIDER = new Rx.Subject()
    /*
     * Setting up the spider's task list
     */
    SPIDER
      // [e]ngage https://schema.org and cheerio full.html!
      // * HELP: notice no Rx.Observable transformation is needed.
      .flatMap(pageUrl => self._net(pageUrl))
      // [l]ist URLs for all the SCHEMA Pages (aka Types of Thing)
      .flatMap($ => {
        // cut back version for testing
        Spider.log("listing")

        var things = $(self._thingsSelector).find(
          "a:not(" + Spider.depth(self._depth + 1) + " a)"
        )
        Spider.log("schemasFound = " + things.length)
        Spider.log("iterating")
        var thingUrls = things.map(function () {
          var url = $(this).attr("href")
          return domain + url
        })
        return Rx.Observable.from(thingUrls)
      })
      // [i]terate SCHEMA URLs and cheerio SCHEMA Pages!
      .flatMap(url => {
        if (url) {
          return self._net(url)
        }
      })
      // [o]utput SCHEMA TYPES the elioWay.
      .map($ => {
        if ($) return self._scrape($)
        else {
          return "No document."
        }
      })
      // WRAP UP: Pack each into super dictionary
      .subscribe(
        value => callback(value),
        error => Spider.log("error[\n\n" + error + "\n\n                ]")
      )

    /*
     * Launch the first leg of the SPIDER ...
     *     .`next`, yes, but actually the .`first` here.
     */
    SPIDER.next("https://SCHEMA.org/docs/full.html")
  }

  /**
   * Returns a Promise to cheerio a schema.org Page.
   * @param {*string} url schema.org url to be scraped
   */
  _net(url) {
    var promise = fetch(url)
      .then(response => response.text())
      .then(body => cheerio.load(body))
    return Rx.Observable.fromPromise(promise)
  }

  /**
   * Static method which saves schema.org's schema as moogoose model files.
   * @param {*string} url Schema.org url to be scraped
   */
  optimize(SCHEMA) {
    // Cos we can't refer to `this` in promises.
    var self = this

    if (!SCHEMA) {
      return "noschema"
    }

    let ejs = require("ejs")

    if (SCHEMA.get("_NAME")) {
      let data = {
        _NAME: SCHEMA.get("_NAME"),
        _TYPES: SCHEMA.get("_TYPES"),
        _EXTENTIONS: SCHEMA.get("_EXTENTIONS"),
      }

      let versionPath = "./endoskeletons/" + self._version

      let pluginFilename =
        versionPath +
        "/models/plugins/" +
        SCHEMA.get("_NAME").toLowerCase() +
        ".js"
      fs.writeFile(
        pluginFilename,
        ejs.render(TEMPLATES["plugin"], data),
        function (err) {
          if (err) {
            Spider.log("pluginFilenameErr[\n\n" + err + "\n\n                ]")
          }
        }
      )

      let modelFilename = versionPath + "/models/" + SCHEMA.get("_NAME") + ".js"
      fs.writeFile(
        modelFilename,
        ejs.render(TEMPLATES["model"], data),
        function (err) {
          if (err) {
            Spider.log("modelFilenameErr[\n\n" + err + "\n\n                ]")
          }
        }
      )
    }
    return Spider.log("optimized[" + SCHEMA.get("_NAME") + "]")
  }

  /**
   * Static method scrapes a schema.org web page.
   * @param {*string} url Schema.org url to be scraped
   */
  _scrape($) {
    // Cos we can't refer to `this` in promises.
    var self = this

    // Build the return value from each of these.
    var _SCHEMA = new Map()
    _SCHEMA.set("_NAME", $("h1.page-title").text().trim())
    _SCHEMA.set("_TYPES", new Map())
    _SCHEMA.set("_EXTENTIONS", Array())

    // Every tbody.
    var schemaTBody = $("table").first().find("tbody")
    schemaTBody.each(function () {
      // Get `tbody` SCHEMA_NAME.
      var schemaName = $(this)
        .children("tr.supertype")
        .last()
        .find("a")
        .first()
        .attr("href")
      // Ignore the `thead` if it has no SCHEMA_NAME.
      if (schemaName) {
        // Check if the "#" prefix for a bookmark url.
        var isBookMark = schemaName.slice(0, 1) === "#"
        // Chop off the "/" prefix.
        schemaName = schemaName.slice(1)
        // If this SCHEMA is the TARGET SCHEMA.
        if (!isBookMark && schemaName === _SCHEMA.get("_NAME")) {
          // The **next** tbody has TARGET SCHEMA_TYPES.
          var schemaRow = $(this).next().find("th.prop-nam")
          // For every row in the *next* tbody (with the SCHEMA_TYPES)...
          schemaRow.each(function () {
            var fieldName = $(this).text().trim()
            var fieldType = self._mongoosify($(this).next().text().trim())
            // Add TYPE to the `_TYPES` section of the SCHEMA dictionary.
            _SCHEMA.get("_TYPES").set(fieldName, fieldType)
          }) // end each table row of the TARGET SCHEMA FIELDS.
        } else {
          // ALl other `tbody` elements are extentions (aka discriminator in mongoose).
          _SCHEMA.get("_EXTENTIONS").push(schemaName)
        } // end if else of TARGET SCHEMA of this page
      } // end if thead has a SCHEMA TITLE.
    }) // end each tbody.
    return _SCHEMA
  }

  /**
   ** Static method which maps the Schema.org Type to a mongoose Model Type.
   * @param {*string} schemaOrgType From `Expected Type` on Schema.org.
   */
  _mongoosify(schemaOrgType) {
    // Always use the first suggestion.
    schemaOrgType = schemaOrgType.split(" or ")[0].trim()
    // Standard mapping.
    var schemap = {
      Boolean: "Boolean",
      Date: "Date",
      DateTime: "Date",
      Number: "Number",
      Float: "mongoose.Schema.Types.Decimal128",
      Integer: "Number",
      Text: "String",
      URL: "String",
      Time: "Date",
    }
    var gander = new Map()
    // If easily mapped.
    if (schemap.hasOwnProperty(schemaOrgType)) {
      // Return a standard `schemap` mongoose Type.
      gander.set("type", schemap[schemaOrgType])
    } else {
      // No map?
      if (this._useOjectFields) {
        // Type is another SCHEMA: Return as mongoose relationship.
        gander.set("type", "mongoose.Schema.Types.ObjectId")
        gander.set("ref", "'" + schemaOrgType.split(" or ")[0].trim() + "'")
      } else {
        gander.set("type", "String")
      }
    }
    return gander
  } // end static method _mongoosify
  /* Example of output: new Map({'type': 'String'}) */

  /**
   ** By increasing the deepness, we can exclude fewer nesting levels.
   * @param {*string} deepness 1 to 5
   */
  static depth(deepness) {
    var s = ""
    while (s.length < deepness * "ul > li > ".length) s += "ul > li > "
    return s
  }
  /* Example of output: Spider.depth(2) == 'ul > li > ul > li > ' */

  /**
   ** Logs messages as method names of the spider artwork.
   * @param {*string} method Like "pausing" or "iconised[file.jpg]"
   */
  static log(method) {
    let msg = "           ." + method
    console.log(msg)
    return msg
  }
}

module.exports = Spider
