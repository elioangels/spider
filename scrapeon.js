/*
 /* The `Spider` scrape runner and handler.
 /*
 */
const mkdirp = require("mkdirp")
const Spider = require("./spider")

var today = new Date()
var version = "ThingOnAString" // today.getFullYear() + '.' + today.getMonth() + '.' + today.getDate()

mkdirp("./endoskeletons/" + version + "/models/plugins/", function (err) {
  if (err) console.log(err)
})

console.log(
  `
      \/ _ \\
    \\_\\(_)/_/ =====(elio)spider
     _//o\\\\_
      /   \\  `
)
var schemon = new Spider(
  (version = "ThingString"),
  (depth = 1),
  (thingsSelector = "#thing_tree"),
  (useOjectFields = false)
)
Spider.log("engaging")
schemon.spider(data => schemon.optimize(data))
