![](https://elioway.gitlab.io/elioangels/elio-spider-logo.png)

# spider ![experimental](/artwork/icon/experimental/favicon.png "experimental")

> Sticky, **Tim Bushell**

NB: This has been retired. We now use **dna** to get the schema data directly from their git repo.

This is a requirement of bones but it can also be run as the boilerplate of a web spidering project with scheming intentions.

## Install

```shell
npm install @elioWay/spider --save
```

## Usage

```javascript
// yourapp.js
const yourAppSpider = require("@elioWay/spider")
var today = new Date()
// Create schemon the spider.
let schemon = new yourAppSpider(
  (version =
    today.getFullYear() + "." + today.getMonth() + "." + today.getDate()), // Do change.
  (depth = 2), // The deeper you go, the more objects you get. Go crazy.
  (thingsSelector = "#thing_tree"), // Don't change - but there is a bigger tree on the page.
  (useOjectFields = true) // Instead of 1 to 1 relationships to other Things, force String type.
)
// Let schemon do spider things.
schemon.spider(
  // Wrap what schemon scraped.
  data => Spider.optimize(data)
)
```

```shell
node yourapp
```

## Seeing is believing

```shell
git clone https://gitlab.com/elioangels/spider.git
cd spider
node test_spider
```

## Credits

- <http://sinonjs.org/>
- <https://github.com/underscopeio/sinon-mongoose>
- <https://cheerio.js.org/>
- <https://stackoverflow.com/questions/34368419/web-scraper-iterating-over-pages-with-rx-js>
- [Spider Ascii Art by Max Strandberg](https://www.asciiart.eu/animals/spiders)

## License

MIT [Tim Bushell](mailto:tcbushell@gmail.com)

[elioWay](https://gitlab.com/elioway/elio/blob/master/README.md)

![](apple-touch-icon.png)
