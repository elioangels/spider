# spider Credits

## Artwork

- [Wolf Spider](https://upload.wikimedia.org/wikipedia/commons/b/b0/Hippasa_holmerae_%28Lawn_wolf_spider%29_in_its_funnel_web_%282%29.jpg)

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)
