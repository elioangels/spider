/*
 * The `Spider` scrape a test version.
 */
const mkdirp = require("mkdirp")
const Spider = require("./spider")

mkdirp("./endoskeletons/TestVersion/models/plugins/", function (err) {
  if (err) console.log(err)
})

console.log(
  `
  #######  \/ _ \\ ########################
 ##      \\_\\(_)/_/          elioSpider  ##
  ######  _//o\\\\_  ######################
           /   \\`
)
var testVersion = new Spider(
  (version = "TestVersion"),
  (depth = 1),
  (thingsSelector = "#thing_tree"),
  (useOjectFields = true)
)
Spider.log("justTesting")
testVersion.spider(data => testVersion.optimize(data))
