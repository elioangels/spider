var ejs = require("ejs")
var fs = require("fs")
var path = require("path")
let chai = require("chai")
chai.should()

describe("ejs templates", function () {
  before(function () {
    SCHEMA = new Map()
    SCHEMA.set("_NAME", "Template")
    SCHEMA.set("_TYPES", new Map())
    var gander = new Map()
    gander.set("type", "Date")
    SCHEMA.get("_TYPES").set("startDate", gander)
    var gander = new Map()
    gander.set("type", "mongoose.Schema.Types.ObjectId")
    gander.set("ref", "'Thing'")
    SCHEMA.get("_TYPES").set("parentObj", gander)
    SCHEMA.set("_EXTENTIONS", Array())
    SCHEMA.get("_EXTENTIONS").push("Document")
    SCHEMA.get("_EXTENTIONS").push("Thing")
    this.data = {
      _NAME: SCHEMA.get("_NAME"),
      _TYPES: SCHEMA.get("_TYPES"),
      _EXTENTIONS: SCHEMA.get("_EXTENTIONS"),
    }
  })

  describe("model", function () {
    it("should render exactly", function () {
      var perfectPath = path.join(
        __dirname,
        ".",
        "mocks",
        "Template.js.rendered"
      )
      var perfect = fs.readFileSync(perfectPath, "utf-8")
      var templatePath = path.join(__dirname, "..", "templates", "model.ejs")
      var model = fs.readFileSync(templatePath, "utf-8")
      rendered = ejs.render(model, this.data)
      rendered.should.eql(perfect)
    })
  })

  describe("plugin", function () {
    it("should render exactly", function () {
      var perfectPath = path.join(
        __dirname,
        ".",
        "mocks",
        "template.js.rendered"
      )
      var perfect = fs.readFileSync(perfectPath, "utf-8")
      var templatePath = path.join(__dirname, "..", "templates", "plugin.ejs")
      var plugin = fs.readFileSync(templatePath, "utf-8")
      rendered = ejs.render(plugin, this.data)
      rendered.should.eql(perfect)
    })
  })
})
