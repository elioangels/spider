exports.thing = {
  name: "Thing 1",
  alternateName: "This is really Thing 1",
  disambiguatingDescription: "This disambiguates Thing 1",
  description: "This describes Thing 1",
}
