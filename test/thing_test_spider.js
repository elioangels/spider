const path = require("path")
const mkdirp = require("mkdirp")
const Spider = require("../spider")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")
var fs = require("fs")

describe("Spider.spider", () => {
  // before(function(done) {
  //   console.log(
  //   `  #######  / _ \\ #####################
  //     ##     \\_\\(_)/_/   elioSpider     ##
  //     ######  _//o\\\\_  ###################
  //              /   \\
  //                   .justTesting()
  //                   .thisDoesntWork = true
  //                   .TODO
  //                   .workaround = `
  //                      Run this on the command line:
  //                        node test_spider
  //                      Then:
  //                        npm test
  //                      ... to test the results.`
  //    )
  //   var schemon = new Spider(
  //     version = 'TestVersion',
  //     depth = 1,
  //     thingsSelector = '#thing_tree',
  //     useOjectFields = true
  //   )
  //   TODO await schemon.spider(
  //     data => console.log(
  //       schemon.wrap(
  //         data
  //       )
  //     )
  //   )
  // })

  it("files should render exactly", () => {
    var modelPath = path.join(
      __dirname,
      "..",
      "endoskeletons/TestVersion/models",
      "Thing.js"
    )
    var perfectPath = path.join(__dirname, ".", "mocks", "Thing.js.rendered")
    var perfect = fs.readFileSync(perfectPath, "utf-8")
    var rendered = fs.readFileSync(modelPath, "utf-8")
    assert.equal(rendered, perfect)

    var pluginPath = path.join(
      __dirname,
      "..",
      "endoskeletons/TestVersion/models/plugins",
      "thing.js"
    )
    var perfectPath = path.join(__dirname, ".", "mocks", "thing.js.rendered")
    var perfect = fs.readFileSync(perfectPath, "utf-8")
    var rendered = fs.readFileSync(pluginPath, "utf-8")
    assert.equal(rendered, perfect)
  })
})
